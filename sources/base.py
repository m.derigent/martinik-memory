import sqlite3

# Connexion à la base de données (le fichier sera créé s'il n'existe pas)
conn = sqlite3.connect('sources/animaux.db')

# Création d'un curseur pour exécuter des commandes SQL
cursor = conn.cursor()

# Exécution du script SQL
script_sql = """
CREATE Table Animaux(
    ID_animal INTEGER PRIMARY KEY,
    Nom VARCHAR(100),
    Espece VARCHAR(100),
    Regime VARCHAR(100),
    Esperence VARCHAR(50),
    Endemique VARCHAR(100)
);

INSERT INTO Animaux(ID_animal,Nom,Espece,Regime,Esperence,Endemique)
VALUES (1,"Colibri","Eulampis jugularis","Nectar des fleurs","3 à 5 ans","Antilles"),
       (2,"Manikou","Didelphis","Fruits, petits vertébrés, vers, insectes","2 ans","Martinique"),
       (3,"Sporophile","Loxigilla noctis","Graines d'herbes","13 mois","Amérique"),
       (4,"Pélican","Pelecanus occidentalis","Poissons","20 ans","Amérique"),
       (5,"Trigonocéphale","Bothrop lanceolatus","Oiseaux, petits rongeurs","20 ans","Martinique"),
       (6,"Anoli","Anolis calolinensis","Petits insectes","10 ans","Martinique"),
       (7,"Tortue molokoï","Chelonoidis carbonarius","Légumes et végétaux","50 ans","Amérique centrale et sud"),
       (8,"Phasme","Phasmoptère","Végetaux (ronce, lierre)","1 à 5 ans","Amérique, Asie et Océanie"),
       (9,"Sémafot(crabe violoniste)","Uca pugilator","Végétaux marins","4 à 7 ans","Amérique"),
       (10,"Mangouste","Herpeste javanicus","Insectes et petits mammifères","10 ans","Amérique et Afrique"),
       (11,"Sphinx du frangipanier","Sphingidae","Feuilles","Quelques mois","Antilles Françaises ou Amérique du sud"), 
       (12,"Buse de swainsoni","Buteo swainsoni","Petits rongeurs, jeunes oiseaux, reptiles, vers","20 ans","Amérique"),
       (13,"Sucrier","Coereba flaveola","Nectar des fleurs","7 ans","Amérique"),
       (14,"Iguane","Iguana iguana","Végétaux, fruits","10 à 15 ans","Amérique"),
       (15,"Kayali","Butorides virescens","Poissons, insectes, invertébrés et amphibiens","8 ans","Amérique centrale et Sud");


CREATE Table mauvaise_reponse(
    ID_animal INTEGER PRIMARY KEY,
    Nom VARCHAR(100),
    Espece VARCHAR(100),
    Regime VARCHAR(100),
    Esperence VARCHAR(50),
    Endemique VARCHAR(100)
);

INSERT INTO mauvaise_reponse(ID_animal,Nom,Espece,Regime,Esperence,Endemique)
VALUES (1,"Paruline jaune","Setophga aestiva","Nectar,suc mielleux des fleurs","4 à 5 ans","Martinique"),
       (2,"Wombat","Marsupialis","Fruits, vers","3 ans","Guadeloupe"),
       (3,"Carouge","Carougus","Nectar des fleurs","15 mois","Martinique"),
       (4,"Albatros","Pelecanus","Poissons, végétaux","15 ans","Martinique"),
       (5,"Vipère heurtante","Ophiophagus","Petits mammiféres, escargots","15 ans","Amérique"),
       (6,"Gecko","Calolinensis","végétaux","11 ans","Guadeloupe"),
       (7,"Tortue de floride","Trachemys scripta","Végétaux","30 ans","Amérique centrale"),
       (8,"Punaise gendarme","Pyrrhocoris apterus","Fruits, petits insectes","1 à 4 ans","Amérique, Océanie"),
       (9,"Crabe touloulou","Gecarcinus lateralis","Végétaux","3 à 7 ans","Martinique"),
       (10,"Langouste","Palinuridae","Mollusques, crustacés","14 ans","Amérique"),
       (11,"Morpho","Lepidoptera","Sève des végétaux","4 mois","Sud des Amériques"),
       (12,"Pyrargue","Haliaetus leucocephalus","Poissons","5 ans","Martinique"),
       (13,"Colibri","Trochilidae","Amphibiens","10 ans","Martinique"),
       (14,"Chaméléon","Chameleo zeylanicus","Vers et poissons","10 à 20 ans","Martinique"),
       (15,"Héron garde-boeufs","Bubulcus ibis","Insectes","7 ans","Amérique du nord");



CREATE Table mauvaise_reponse2(
    ID_animal INTEGER PRIMARY KEY,
    Nom VARCHAR(100),
    Espece VARCHAR(100),
    Regime VARCHAR(100),
    Esperence VARCHAR(50),
    Endemique VARCHAR(100)
);

INSERT INTO mauvaise_reponse2(ID_animal,Nom,Espece,Regime,Esperence,Endemique)
VALUES (1,"Sucrier","Eulampis jungularis","Insectes","3 à 6 ans","Amérique"),
       (2,"Coati","Nasua narica","Fruits, insectes, végétaux","5 ans","Amérique du sud"),
       (3,"Merle","Turdus merula","Graines de fruits, nectar des fleurs","12 mois","Guyane"),
       (4,"Frégate","Pelecanus orientalis","Insectes, végétaux","25 ans","Antilles"),
       (5,"Mamba noir","Bothrops lancealatus","Petits mammifères et batraciens","21 ans","Antilles"),
       (6,"Mabouya","Mabuya mabouya","Petits insectes, végétaux","12 ans","Amérique"),
       (7,"Tortue caouanne","carbonarius","Poissons et végétaux","40 ans","Amérique"),
       (8,"Mante religieuse","Mantis religiosa","Végetaux (feuilles, lierre)","1 à 6 ans","Amérique, Asie, Océanie et Europe"),
       (9,"Cirique","Ucas pugilator","Végétaux marins, petits poissons","4 à 8 ans","Antilles"),
       (10,"Suricate","Suricata suricatta","Végétaux","13 ans","Amérique du sud"),
       (11,"Monarque","Danaus plexippus","Nectar des fleurs","Quelques semaines","Antilles"),
       (12,"Harpie féroce","Harpia harpyja","Végétaux, escargots, vers de terre...","21 ans","Amérique du sud"),
       (13,"Buse","Buteo swaisoni","Petits rongeurs","5 ans","Antilles"),
       (14,"Basilic","Basiliscus plumifrons","Petits insectes","11 à 16 ans","Antilles"),
       (15,"Grand héron","Linnaeus","Insectes, invertébrés et amphibiens","6 ans","Antilles françaises");

"""

cursor.executescript(script_sql)

# Valider les modifications et fermer la connexion
conn.commit()
conn.close()
