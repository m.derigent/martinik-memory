Jeu de memoire 
==========



Ce jeu est constitué de 3 modes:
- le mode 1 et 2 (jeu de cartes)
- le mode 3 (quiz)

Mode 1 :
-------
Le but est de créer des paires le plus rapidement possible.
Une paire se constitue quand 2 cartes identiques sont retournées.

Mode 2 :
-------
Même but, mais les paires sont constituées d'une carte avec l'image d'un 
animal et l'autre avec son nom.

Mode 3 :
-------
Le but est de répondre le plus de bonnes réponses possible.
Vous avez un temps limité pour retenir les informations des animaux, et les questions 
sont aléatoires, accompagnées d'une image qui lui correspond.
Il y a 3 choix possibles pour les réponses, donc choisissez celle qui est correcte.

Il est aussi possible de choisir le nombre de cartes et de questions dans l'accueil,
donc veuillez utiliser la scrollbar située à droite de la fenêtre pour sélectionner
le mode auquel vous souhaitez jouer.

Une fois votre partie terminée, vous pouvez entrer votre nom d'utilisateur pour 
enregistrer votre score.

Le classement et les règles du jeu se trouvent en haut de la page d'accueil, où apparaissent 
les scores des joueurs et les règles du jeu détaillées.



Utilisation
-----------

- Exécutez `main.py` pour démarrer le jeux

## Installation

Pour installer le jeu, suivez ces étapes :

    Ouvrez un terminal sur votre ordinateur.
    Utilisez la commande git clone pour cloner le dépôt du jeu. Assurez-vous d'avoir Git installé sur votre système. Voici un exemple de commande pour cloner le dépôt :



git clone <URL_du_dépôt>

Remplacez <URL_du_dépôt> par l'URL du dépôt que vous souhaitez cloner.

    Une fois le dépôt cloné, accédez au répertoire du jeu à l'aide de la commande cd :



cd nom_du_répertoire_du_jeu

Assurez-vous de remplacer nom_du_répertoire_du_jeu par le nom du répertoire dans lequel vous avez cloné le dépôt.

    Ensuite, vous devez installer les dépendances du jeu. Utilisez pip pour installer les dépendances à partir du fichier requirements.txt. Voici la commande à exécuter :

pip install -r requirements.txt

Cela installera toutes les dépendances nécessaires pour exécuter le jeu sur votre système.

Une fois ces étapes terminées, vous devriez être prêt à exécuter le jeu. 
